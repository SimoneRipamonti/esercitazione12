package serializzazione;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public final static int PORT = 29999;

	public void startServer() throws IOException, ClassNotFoundException {
		ServerSocket serverSocket = new ServerSocket(PORT);
		Socket socket = serverSocket.accept();

		ObjectInputStream inputStream = new ObjectInputStream(
				socket.getInputStream());

		/**
		 * In automatico deserializza il player. I due oggetti sono uguali come
		 * contenuto, ma sono due oggetti distinti, infatti uno si trova sulla
		 * virtual machine del client e l'altro su quella del server
		 */

		Player player = (Player) inputStream.readObject();

		System.out.println("SERVER: " + player);
	}

	public static void main(String[] args) throws ClassNotFoundException,
			IOException {
		Server server = new Server();
		server.startServer();
	}
}
