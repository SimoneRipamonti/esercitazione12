package serializzazione;

import java.io.Serializable;

/**
 * Serializzazione: anzich� passare una stringa, creo un oggetto "Azione" sul
 * client e mando l'oggetto al server. Prende un oggetto e lo trasforma in una
 * stringa di byte. Tutti gli attributi di un oggetto serializzabile devono
 * essere serializzabili.
 * 
 * 
 * Il client crea un'azione e quindi fa azione.esegui(), si vuole che venga
 * modificato lo stato dell'oggetto che sta sul server e non sul client
 * 
 * attributo transient non viene serializzato, alla deserializzazione ottiene il
 * valore di default
 * 
 * @author Simone
 *
 */
public class Player implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + "]";
	}

}
