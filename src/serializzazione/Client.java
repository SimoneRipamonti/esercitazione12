package serializzazione;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	private final static int PORT = 29999;

	public void startClient() throws UnknownHostException, IOException {
		Socket socket = new Socket("127.0.0.1", PORT);
		System.out.println("CLIENT: connesso");
		
		/**
		 * Anzich� un PrintWriter, uso un ObjectOutputStream
		 */
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		Scanner stdin = new Scanner(new InputStreamReader(System.in));
		
		while (true){
			String inputLine = stdin.nextLine();
			Player player = new Player (inputLine);
			objectOutputStream.writeObject(player);
			objectOutputStream.flush();
		}
	}
	
	public static void main(String [] args) throws UnknownHostException, IOException{
		Client client = new Client();
		client.startClient();
	}
}
