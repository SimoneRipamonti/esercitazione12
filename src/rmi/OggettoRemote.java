package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * Definisco lo stub per il client
 * @author Simone
 *
 */
public interface OggettoRemote extends Remote {
	public void changeName(String name) throws RemoteException;
}
