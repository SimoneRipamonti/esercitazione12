package rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

	private final static String HOST = "127.0.0.1";
	private final static int PORT = 1099;

	private final static String NAME = "room";

	public static void main(String[] args) throws RemoteException,
			NotBoundException {
		/**
		 * Prende il registro sull'HOST
		 */
		Registry registry = LocateRegistry.getRegistry(HOST, PORT);
		/**
		 * Crea lo stub, tramite una lookup. room equivale a room del server
		 */
		RMIRoomRemote game = (RMIRoomRemote) registry.lookup(NAME);

		game.printStato();
		/**
		 * Ritorna uno stub
		 */
		OggettoRemote object = game.getObject();
		/**
		 * Contatta il server e cambia lo stato dell'oggetto sul server
		 */
		object.changeName("telefono");
		game.printStato();

		OggettoRemote oggetto = new Oggetto("Spadad");
		game.cambiaOggetto(oggetto);
		/**
		 * Il metodo toStrnig dell'oggetto farlocco non � un metodo remoto,
		 * quindi viene chiamato il metodo toString di default della classe
		 * object. L'oggetto farlocco � sul client, il client ha lo skeleton e
		 * il server ha lo stub, il server chiama il metodo sullo stub (che non
		 * espone il toString come metodo remoto)
		 */
		game.printStato();

		// Oggetto oggetto = new Oggetto("spada");
		// game.cambiaOggetto(oggetto);
		// game.printStato();
		// /**
		// * Non cambia il nome sul server, perch� oggetto � un oggetto locale e
		// * non remoto. L'unico oggetto che posso modificare del server � game
		// */
		// oggetto.changeName("cubo");
		// game.printStato();
		// /**
		// * Ora cambia l'oggetto sul server, perch� viene passato tramite
		// cambiaOggetto()
		// */
		// game.cambiaOggetto(oggetto);
		// game.printStato();
	}

}
