package rmi;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {

	private final Registry registry;

	/**
	 * Nome per l'interfaccia che espone sul registro
	 */
	private static final String NAME = "room";

	public Server() throws RemoteException, AlreadyBoundException {
		registry = LocateRegistry.createRegistry(1099);

		RMIRoom game = new RMIRoom();
		/**
		 * Esporto nel registro la mia interfaccia locale
		 */
		RMIRoomRemote gameRemote = (RMIRoomRemote) UnicastRemoteObject
				.exportObject(game, 0);
		/**
		 * bind nome e remote
		 */
		registry.bind(NAME, gameRemote);
	}
	
	public static void main (String [] args) throws RemoteException, AlreadyBoundException{
		new Server();
	}
}
