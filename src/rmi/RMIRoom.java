package rmi;

import java.rmi.RemoteException;

public class RMIRoom implements RMIRoomRemote {

	private OggettoRemote oggetto;
	
	public RMIRoom() throws RemoteException{
		oggetto = new Oggetto("default object");
	}
	
	@Override
	public void printStato() throws RemoteException {
		System.out.println(oggetto);
		
	}

	@Override
	public void cambiaOggetto(OggettoRemote oggetto) throws RemoteException {
		this.oggetto = oggetto;
		
	}

	@Override
	public OggettoRemote getObject() throws RemoteException {
		return oggetto;
	}

}
