package rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Oggetto mandato avanti e indietro dal server
 * 
 * @author Simone
 *
 */
public class Oggetto extends UnicastRemoteObject implements OggettoRemote {

	private static final long serialVersionUID = 1L;
	
	private String name;

	public Oggetto(String name) throws RemoteException {
		super();
		this.name = name;
	}

	public void changeName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Oggetto [name=" + name + "]";
	}
}
