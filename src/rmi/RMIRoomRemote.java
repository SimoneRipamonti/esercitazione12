package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 * Interfaccia che verr� pubblicata, skeleton
 * Tutti i metodi invocabili da remoto lanciano l'eccezione RemoteException
 * @author Simone
 *
 */
public interface RMIRoomRemote extends Remote {
	public void printStato() throws RemoteException;
	public void cambiaOggetto(OggettoRemote oggetto) throws RemoteException;
	public OggettoRemote getObject() throws RemoteException;
	
}
